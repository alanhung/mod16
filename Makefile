all: lint fmt

src=lua

lint: $(src)
	@luacheck $(src)

fmt: $(src)
	@stylua $(src)

doc: $(src)
	@ldoc $(src) -a -d ldoc
