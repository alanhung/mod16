# mod16

A 16-colored dark neovim theme for the linux tty.

## Usage

Enable the colorscheme:

```vim
colorscheme mod16
```

```lua
require("mod16").setup()
```
