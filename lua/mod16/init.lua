--- top level plugin module
-- @module mod16

local theme = require("mod16.theme")

--- highlight a highlight group
-- @param hi_name name of highlight group
-- @param hi table of highlight group's fg and/or bg color
local function highlight(hi_name, hi)
    local fg = hi.fg and "ctermfg=" .. hi.fg or "ctermfg=None"
    local bg = hi.bg and "ctermbg=" .. hi.bg or "ctermbg=None"
    return string.format("hi %s %s %s", hi_name, fg, bg)
end

--- highlight theme
-- @param his table of highlight groups
local function highlight_all(his)
    for hi_name, hi in pairs(his) do
        vim.cmd(highlight(hi_name, hi))
    end
end

local init = {}

--- setup function
init.setup = function()
    highlight_all(theme)
end

return init
