--- theme module
-- @module theme

local c = require("mod16.colors")

--- table of highlight group colors
local theme = {
    Comment = { fg = c.gray1 },
    ColorColumn = { fg = c.gray0 },
    Conceal = { fg = c.gray1 },
    Cursor = { fg = c.black, bg = c.gray0 },
    lCursor = { fg = c.black, bg = c.gray0 },
    CursorIM = { fg = c.black, bg = c.gray0 },

    -- CursorColumn = {},
    -- CursorLine = {},

    Directory = { fg = c.blue1 },
    DiffAdd = { bg = c.green0 },
    DiffChange = { bg = c.blue0 },
    DiffDelete = { bg = c.red0 },
    DiffText = { bg = c.yellow0 },

    EndOfBuffer = { fg = c.blue0 },

    TermCursor = { fg = c.black, bg = c.white },
    TermCursorNC = { fg = c.black, bg = c.white },

    ErrorMsg = { fg = c.red1 },
    WinSeparator = { bg = c.gray1 },
    Folded = { fg = c.blue1 },
    FoldColumn = { bg = c.black },
    SignColumn = { bg = c.black },

    IncSearch = { bg = c.black, fg = c.yellow1 },
    Substitute = { bg = c.yellow1, fg = c.black },

    LineNr = { fg = c.white },
    LineNrAbove = { fg = c.gray0 },
    LineNrBelow = { fg = c.gray0 },
    CursorLineNr = { fg = c.white },
    CursorLineSign = { bg = c.black },
    CursorLineFold = { bg = c.black },

    MatchParen = { bg = c.cyan0, fg = c.white },
    ModeMsg = { fg = c.gray0 },
    MsgArea = { bg = c.black },
    MsgSeparator = { bg = c.gray1 },
    MoreMsg = { fg = c.gray0 },
    NonText = { fg = c.blue1 },

    Normal = { bg = c.black },
    NormalFlaot = { bg = c.black },
    NormalNC = { bg = c.black },

    Pmenu = { fg = c.gray0, bg = c.black },
    PmenuSel = { fg = c.white, bg = c.blue1 },
    PmenuSBar = { bg = c.gray0 },
    PmenuThumb = { bg = c.blue0 },

    Question = { fg = c.blue1 },
    QuickFixLine = { fg = c.black, bg = c.yellow1 },
    Search = { fg = c.black, bg = c.yellow1 },

    SpecialKey = { fg = c.gray1 },
    SpellBad = { fg = c.red0 },
    SpellCap = { fg = c.cyan0 },
    SpellLocal = { fg = c.yellow0 },

    SpellRare = { fg = c.magenta0 },
    StatusLine = { bg = c.white, fg = c.black },
    StatusLineNC = { bg = c.gray1, fg = c.black },

    TabLine = { bg = c.gray1, fg = c.gray0 },
    TabLineFill = { bg = c.gray1, fg = c.gray0 },
    TabLineSel = { fg = c.black, bg = c.white },
    Title = { fg = c.green1 },
    Visual = { bg = c.blue0 },
    VisualNOS = { bg = c.blue0 },
    WarningMsg = { fg = c.red1 },
    Whitespace = {},
    Wildmenu = { fg = c.black, bg = c.blue1 },

    FloatBorder = { bg = c.black, fg = c.white },

    Constant = { fg = c.cyan11 },
    String = { fg = c.green1 },
    Character = { fg = c.green1 },
    Number = { fg = c.cyan1 },
    Boolean = { fg = c.cyan1 },
    Float = { fg = c.yellow1 },
    Identifier = { fg = c.red1 },
    Function = { fg = c.blue1 },
    Statement = { fg = c.magenta1 },
    Conditional = { fg = c.magenta1 },
    Repeat = { fg = c.magenta1 },
    Label = { fg = c.magenta1 },
    Operator = { fg = c.magenta1 },
    Keyword = { fg = c.magenta1 },
    Exception = { fg = c.magenta1 },

    PreProc = { fg = c.magenta1 },
    -- Include       = { },
    -- Define        = { },
    -- Macro         = { },
    -- PreCondit     = { },

    Type = { fg = c.yellow1 },
    -- StorageClass  = { },
    -- Structure     = { },
    -- Typedef       = { },
    --
    Special = { fg = c.red1 },
    -- SpecialChar   = { },
    -- Tag           = { },
    -- Delimiter     = { },
    -- SpecialComment= { },
    -- Debug         = { },

    -- Underlined = { },
    -- Bold = { },
    -- Italic = { },
    -- Ignore = { },

    Error = { fg = c.red1 },
    Todo = { fg = c.black, bg = c.yellow1 },
    -- qfLineNr = { fg = c.white },
    -- qfFileName = { fg = c.blue1 },
    -- htmlTag = { fg = c.gray0 },

    -- TSAnnotation = { },
    -- TSAttribute = { },
    -- TSBoolean = { },
    -- TSCharacter = { },
    -- TSComment = { fg = c.gray1 },
    -- TSNote = { },
    -- TSWarning = { },
    -- TSDanger = { },
    ["@constructor"] = { fg = c.blue1 },
    -- TSConditional = { },
    -- TSConstant = { fg = c.cyan1 },
    -- TSConstBuiltin = { fg = c.cyan1},
    -- TSConstMacro = { fg = c.cyan1 },
    -- TSError = { },
    -- TSException = { },
    ["@field"] = { fg = c.red1 },
    -- TSFloat = { },
    -- TSFunction = { fg = c.blue1 },
    -- TSFuncBuiltin = { fg = c.blue1 },
    -- TSFuncMacro = { fg = c.blue1 },
    -- TSInclude = { fg = c.magenta1 },
    ["@keyword"] = { fg = c.magenta1 },
    ["@label"] = { fg = c.green1 },
    -- TSMethod = { },
    ["@namespace"] = { fg = c.gray0 },
    -- TSNone = { },
    -- TSNumber = {}
    -- TSOperator = {}
    ["@parameter"] = { fg = c.red1 },
    -- TSParameterReference = {}
    ["@property"] = { fg = c.red1 },
    ["@punctuation.delimiter"] = { fg = c.gray0 },
    ["@punctuation.bracket"] = { fg = c.gray0 },
    ["@punctuation.special"] = { fg = c.gray0 },
    -- TSRepeat = {}
    -- TSString = {}
    -- TSStringRegex = {}
    ["@string.escape"] = { fg = c.red1 },
    ["@symbol"] = {fg = c.cyan1},
    -- TSType = {}
    -- TSTypeBuiltin = {},
    ["@variable"] = { fg = c.gray0 },
    ["@variable.builtin"] = { fg = c.gray0 },
    ["@tag"] = { fg = c.red1 },
    -- TSTagAttribute = { fg = c.gray0 },
    -- TSTagDelimiter = {}
    -- TSText
    -- TSEmphasis
    -- TSUnderline
    -- TSStrike
    -- TSTitle
    -- TSLiteral
    -- TSURI
}

return theme
