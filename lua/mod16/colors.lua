--- color module
-- @module color

--- table of colors
local c = {
    black = "0", -- black
    blue0 = "DarkBlue", -- dark blue
    green0 = "DarkGreen", -- dark green
    cyan0 = "DarkCyan", -- dark cyan
    red0 = "DarkRed", -- dark red
    magenta0 = "DarkMagenta", -- dark magenta
    yellow0 = "DarkYellow", -- dark yellow
    gray0 = "Gray", -- gray
    gray1 = "8", -- dark gray
    blue1 = "Blue", -- blue
    green1 = "Green", -- green
    cyan1 = "Cyan", -- cyan
    red1 = "Red", -- red
    magenta1 = "Magenta", -- magenta
    yellow1 = "Yellow", -- yellow
    white = "15", -- white
}

return c
